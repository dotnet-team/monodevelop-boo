monodevelop-boo (2.4-2) unstable; urgency=low

  * [131e20e] Add an Uploaders field with me in it, and set the package
    to team-maintained.

 -- Jo Shields <directhex@apebox.org>  Mon, 13 Sep 2010 15:13:10 +0100

monodevelop-boo (2.4-1) experimental; urgency=low

  * The "my brain hurts!" release
  * New upstream release
  * debian/rules:
    + Don't use ../tarballs for get-orig-source
  * debian/source/format:
    + Force DebSrc 1.0
  * debian/control:
    + Bump build-deps to MonoDevelop 2.4
    + Don't use version number hack in dependencies, since it's 2.4 everywhere
    + Remove build-dep on GtkSourceview
  * debian/patches/allow_minimal_configure_for_clean.dpatch:
    + Refreshed
  * debian/patches/build_against_gtksourceview2-sharp.dpatch:
    + Remove patch (GtkSourceview is no longer used)

 -- Jo Shields <directhex@apebox.org>  Wed, 16 Jun 2010 22:04:16 +0100

monodevelop-boo (2.2-3) unstable; urgency=low

  * Rebuild against MonoDevelop 2.2.2
  * debian/control:
    + Build-depend on MonoDevelop 2.2.2
    + Hack binary package dependencies to add ".2" to expected version
      of Monodevelop - annoyingly, this is required on rare occasions like
      this where addins don't have matching versions to MD itself.

 -- Jo Shields <directhex@apebox.org>  Thu, 10 Jun 2010 09:43:13 +0100

monodevelop-boo (2.2-2) unstable; urgency=low

  * Rebuild against MonoDevelop 2.2.1
  * debian/control:
    + Bump Standards to 3.8.4 (no changes)
    + Build-depend on MonoDevelop 2.2.1
    + Hack binary package dependencies to add ".1" to expected version
      of Monodevelop - annoyingly, this is required on rare occasions like 
      this where addins don't have matching versions to MD itself.

 -- Jo Shields <directhex@apebox.org>  Thu, 11 Feb 2010 11:28:21 +0000

monodevelop-boo (2.2-1) unstable; urgency=low

  [ Jo Shields ]
  * New upstream release
  * debian/rules:
    + Deterministic get-orig-source rule, persistent md5sum created
  * debian/control:
    + Use -dev packages
    + Use mono-devel 2.4.3 and remove individual library build-deps
  * debian/patches/build_against_gtksourceview2-sharp.dpatch:
    + Refreshed

  [ Stefan Ebner ]
  * debian/control:
    + Get rid of libgconf2.24-cil since gnome-sharp is fixed now in ubuntu
    + Bump Standards-Version to 3.8.3

 -- Jo Shields <directhex@apebox.org>  Mon, 11 Jan 2010 14:01:07 +0000

monodevelop-boo (2.0-1) unstable; urgency=low

  [ Jo Shields ]
  * New upstream release
  * debian/control:
    + Tweak build-deps to allow building against libgconf2.24-cil due to 
      broken ABI bump in Ubuntu
  * debian/patches/build_against_gtksourceview2-sharp.dpatch,
    debian/control:
    + Port from old GTKSourceView API to current version - thanks to
      Peter Johanson for his help

  [ Iain Lane ]
  * Bump MD build-dep to >= 2.0 (configure checks) 

 -- Jo Shields <directhex@apebox.org>  Wed, 08 Apr 2009 22:42:33 +0100

monodevelop-boo (1.9.3-1) unstable; urgency=low

  [ Mirco Bauer ]
  * New upstream (bugfix) release.
  * debian/control:
    + Tighten binary dependency on monodevelop, to ensure the plugin is only
      installed using the same monodevelop version, as the plugins are not
      forward compatible. (Closes: #520545)
  * debian/watch:
    + Updated to use directory of ftp/http download server.

  [ Jo Shields ]
  * debian/patches/allow_minimal_configure_for_clean.dpatch,
    debian/rules:
    + Allow source package to be created without installing MD first
  * debian/control:
    + We need Boo 0.8.2 as a minimum, to prevent FTBFS

  [ Stefan Ebner ]
  * Bump Standards version to 3.8.1

 -- Mirco Bauer <meebey@debian.org>  Sun, 22 Mar 2009 14:42:15 +0100

monodevelop-boo (1.9.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Mirco Bauer <meebey@debian.org>  Mon, 02 Mar 2009 21:33:54 +0100

monodevelop-boo (1.9.2-1) experimental; urgency=low

  [ Iain Lane ]
  * New upstream version
  * debian/control:
    + Depend on Monodevelop 2.0 versions
  * debian/patches/shellpanel_ftbfs_fix.dpatch:
    + Removed, merged upstream

  [ Mirco Bauer ]
  * debian/rules:
    + Implemented get-orig-source target.
  * debian/watch:
    + Updated for unstable releases.

 -- Mirco Bauer <meebey@debian.org>  Wed, 18 Feb 2009 20:12:22 +0100

monodevelop-boo (1.0-3) unstable; urgency=low

  * debian/patches/shellpanel_ftbfs_fix.dpatch: Fix FTBFS by backporting
    upstream changes

 -- Iain Lane <laney@ubuntu.com>  Mon, 22 Dec 2008 01:09:06 +0000

monodevelop-boo (1.0-2) unstable; urgency=low

  * Rebuild against against libgtksourceview2.0-cil 0.12-1
  * debian/control:
    + Added the boo compiler to dependencies.

 -- Mirco Bauer <meebey@debian.org>  Fri, 02 May 2008 15:03:16 +0200

monodevelop-boo (1.0-1) unstable; urgency=low

  * New upstream release
  * Updated monodevelop build-dependency to >= 1.0

 -- Mirco Bauer <meebey@debian.org>  Thu, 28 Feb 2008 22:59:01 +0100

monodevelop-boo (0.19-1) unstable; urgency=low

  * New upstream release

 -- Mirco Bauer <meebey@debian.org>  Sun, 24 Feb 2008 19:10:19 +0100

monodevelop-boo (0.18.1-1) unstable; urgency=low

  * Initial packaging, partly based on monodevelop-boo of the monodevelop
    source package.

 -- Mirco Bauer <meebey@debian.org>  Sun, 10 Feb 2008 16:58:49 +0100
